import requests
import json

# url = 'http://127.0.0.1:8888/invocations'
url = 'http://91.233.28.155:8003/invocations'
file = {'file': open('test_data.csv', 'rb')}
resp = requests.post(url=url, files=file)
print(resp.json())

with open('test.json', 'w', encoding='utf-8') as f:
    json.dump(resp.json(), f, ensure_ascii=False, indent=4)

