import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile


load_dotenv()
app = FastAPI()

ip = os.getenv("MLFLOW_TRACKING_IP")
user_name = os.getenv("MLFLOW_USER")
user_password = os.getenv("MLFLOW_PASSWORD")
remote_server_uri = f"http://{user_name}:{user_password}@{ip}:80"
print(remote_server_uri)
mlflow.set_tracking_uri(remote_server_uri)


class Model:
    def __init__(self, model_name, model_stage):
        """
        Main model Class
        :param model_name: name from the server MLFlow http://91.233.28.155/
        :param model_stage: None/Staging/Production/Archived
        """
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        """
        Predict function
        :param data: DataFrame
        :return: DataFrame
        """
        predictions = self.model.predict(data)
        return predictions


model = Model("xgb-live", "Staging")


@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    """
    Uploading files with data
    :param file: *.csv
    :return: json
    """
    try:
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(
            file.filename, index_col=["shop_id", "base_sku", "date_local"]
        )
        pred = pd.DataFrame({"prediction": model.predict(data)}, index=data.index)

    except Exception:
        return {"Error": "Uploading error or this file is not *.csv"}

    finally:
        file.file.close()

    return pred.to_json()


@app.post("/ping")
def pong():
    return "Hi!"


if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(1)
